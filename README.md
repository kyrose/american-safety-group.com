# [american-safety-group.com](american-safety-group.com)
<!-- [![Netlify Status](https://api.netlify.com/api/v1/badges/124f716e-6b7e-4137-b917-f2a21d797058/deploy-status)](https://app.netlify.com/sites/american-safety-group/deploys) -->

[![Netlify Status](https://api.netlify.com/api/v1/badges/4ebd8f50-1c76-4d70-ac51-973a366cc14f/deploy-status)](https://app.netlify.com/sites/american-safety-group/deploys)

A Jekyll rebuild of American Safety Group's WordPress site.
